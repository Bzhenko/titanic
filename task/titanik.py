import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    mr_missing = df[df['Name'].str.contains('Mr.')]['Age'].isnull().sum()
    mr_median = df[df['Name'].str.contains('Mr.')]['Age'].median() # float
    mr_median = int(mr_median)
    
    mrs_missing = df[df['Name'].str.contains('Mrs.')]['Age'].isnull().sum() 
    mrs_median = df[df['Name'].str.contains('Mrs.')]['Age'].median() # float
    mrs_median = int(mrs_median)

    miss_missing = df[df['Name'].str.contains('Miss.')]['Age'].isnull().sum()
    miss_median = df[df['Name'].str.contains('Miss.')]['Age'].median() # float
    miss_median = int(miss_median)
    
    return [('Mr.', mr_missing, mr_median), 
            ('Mrs.', mrs_missing, mrs_median),
            ('Miss.', miss_missing, miss_median)]
